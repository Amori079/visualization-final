window.addEventListener('load', function() {
  var Example = Example || {};
	//Fetch our canvas
	var canvas = document.getElementById('world');

	//Setup Matter JS
	var engine = Matter.Engine.create();
	var world = engine.world;
	var render = Matter.Render.create({
		canvas: canvas,
		engine: engine,
		options: {
			width: 1500,
			height: 1500,
			background: 'transparent',
			wireframes: false,
			showAngleIndicator: true
		}
	});

	//Add a ball
	var ball = Matter.Bodies.circle(250, 250, 25, {
		density: 0.5,
		friction: 0.5,
    restitution: .9,
    render: {
        fillStyle: '#F35e66',
        lineWidth: 1
    }
});

	Matter.World.add(world, ball);


	//Add a floor
	var floor = Matter.Bodies.rectangle(150, $(window).height(), $(window).width(), 50, {
		isStatic: true, //An immovable object
		render: {
			visible: true
		}
	});
	Matter.World.add(world, floor);

	//Make interactive
	var mouseConstraint = Matter.MouseConstraint.create(engine, { //Create Constraint
		element: canvas,
		constraint: {
			render: {
	        	visible: false
	    	},
	    	stiffness:0.8
	    }
	});
  mouseConstraint.mouse.element.removeEventListener("mousewheel", mouseConstraint.mouse.mousewheel);
  mouseConstraint.mouse.element.removeEventListener("DOMMouseScroll", mouseConstraint.mouse.mousewheel);
	Matter.World.add(world, mouseConstraint);

	//Start the engine
	Matter.Engine.run(engine);
	Matter.Render.run(render);

});
