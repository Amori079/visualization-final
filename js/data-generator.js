//load county data
// d3.json("https://api.census.gov/data/2017/pep/components?get=BIRTHS,DEATHS,GEONAME&for=state:*&PERIOD=7", function(error, jsonData){
//    console.log(jsonData);
// });

var lifes = []
var started = false;
var countydata;
var sumPopulation;

var myPromise = new Promise(function(resolve,reject){
	//data source:    https://simplemaps.com/data/us-cities
    d3.csv("data/uscitiesv1.5.csv",function(error,data){
      // console.log(data);
      countydata = data.filter(function(d){return d.population != "";});
      // console.log(countydata);
      sumPopulation = 0;

      countydata.forEach(function(d){
        d.lowerBound = sumPopulation;
        sumPopulation = sumPopulation + Number(d.population);
        d.upperBound = sumPopulation;
      });

    });
  setTimeout(resolve, 0);
});

async function setup() {

  // await blocks until the promise is resolved
	await myPromise;
	started = true;
}

var death = 0;
function draw() {
  if(started){
    // noLoop();
    //original 8, 11
    var lifeRate = 1;
    var deathRate = 3;
    var lifeStd = Math.sqrt(lifeRate);
    var deathStd = Math.sqrt(deathRate);
    var lifeRandom = randomGaussian(lifeRate,lifeStd);
    var deathRandom = randomGaussian(deathRate,deathStd);

    if(death%2==1){
      newDeath();
    }else{
      newBirth();
    }

    frameRate(1/((1-death%2)*lifeRandom+(death%2)*(deathRandom-lifeRandom)));
    death = death+1;
  }
};

// create birth function
function newBirth(){
    var random = Math.floor(Math.random()*sumPopulation)
    var selectedCounty;
    // random selection of data
    countydata.forEach(function(d){
      if (d.lowerBound <= random && random < d.upperBound ){
        selectedCounty = d;
      }
    })

    console.log("new Birth");
    var birth = {
      type:"birth",
      county:selectedCounty.county_name,
      state:selectedCounty.state_name,
      lattitude: selectedCounty.lat,
      longitude: selectedCounty.lng,
      population:selectedCounty.population,
      timezone: selectedCounty.timezone
    };
    //updateVisualization(birth);
    updateData(birth);
    setTimeout(processLife,3000);
}

// create death function
function newDeath(){
  var random = Math.floor(Math.random()*sumPopulation)
  var selectedCounty;
  // random selection of data
  countydata.forEach(function(d){
    if (d.lowerBound <= random && random < d.upperBound ){
      selectedCounty = d;
    }
  })

    console.log("new Death");
    var death = {
      type:"death",
      county:selectedCounty.county_name,
      state:selectedCounty.state_name,
      lattitude: selectedCounty.lat,
      longitude: selectedCounty.lng,
      population:selectedCounty.population,
      timezone: selectedCounty.timezone
    };
    // updateVisualization(death);
    updateData(death);
    setTimeout(processLife,3000);
}

// updateVisualization
function updateData(life){
  lifes.push(life);
  updateVisualization(lifes);

}

// processLife();

function processLife() {
	// remove the first item of an array
	lifes.shift();
	updateVisualization(lifes);
	// setTimeout(processLife, 1000);
}
