// The function is called every time when an order comes in or an order gets processed
// The current order queue is stored in the variable 'orders'

// The function is called every time when an life comes in or an life gets processed
// The current life queue is stored in the variable 'orders'
var width = 1000;
var height = 500;

var svg = d3.select("#life-map").append("svg")
.attr("width",width)
.attr("height",height);

// draw U.S. map

var projection = d3.geoAlbersUsa()
                   .translate([width/2, height/2])
                   .scale([1000]);

//
var path = d3.geoPath()
             .projection(projection);

//Load in GeoJSON data
d3.json("data/us-states.json", function(json) {
    console.log(json);
    //Bind data and create one path per GeoJSON feature
    svg.selectAll("path")
       .data(json.features)
       .enter()
       .append("path")
       .attr("d", path)
       .style("fill","rgb(211,211,211)");
});

// update when lifes changes
var lifeList = [];
var birthList = [];
var deathList = [];

function updateVisualization(lifes) {
 // life = lifes.pop();
 console.log(lifes);

  // if(life.type=="birth"){
  //   lifeList.push(life);
  //   birthList.push(life);
  //   console.log(birthList);
  // }
  // if(life.type=="death"){
  //   lifeList.push(life);
  //   deathList.push(life);
  //   console.log(deathList);
  // }

  // Data-join (circle now contains the update selection)
  var circle = svg.selectAll("circle").data(lifes,function(d) { return d; });


  //
  circle
  .exit()
  .transition()
  // .duration(3000)
  .attr("r",50)
  .style("fill","rgba(0,0,0,0)")
  .remove();

  // Update (set the dynamic properties of the elements)
   circle.enter().append("circle")
         .attr("r", 20)
         .attr("cx", function(d) { return projection([d.longitude,d.lattitude])[0];})
         .attr("cy", function(d) { return projection([d.longitude,d.lattitude])[1];})
         .attr("class",function(d){
            return d.type;
          })
          ;



  // animation();

}

// function animation(){
//   svg.selectAll("circle").transition().duration(3000).attr("r",50)
//   .style("fill","rgba(0,0,0,0)")
//   .remove();
// }

//set all of the sound clips
var lifeAudio = [];
// lifeAudio[0]=new Audio();
// lifeAudio[0].src='/sounds/g-major-a3.wav';
//
// lifeAudio[1]=new Audio();
// lifeAudio[1].src='/sounds/g-major-a4.wav';
//
// lifeAudio[2]=new Audio();
// lifeAudio[2].src='/sounds/g-major-b3.wav';
//
// lifeAudio[3]=new Audio();
// lifeAudio[3].src='/sounds/g-major-b4.wav';
//
// lifeAudio[4]=new Audio();
// lifeAudio[4].src='/sounds/g-major-d3.wav';
//
// lifeAudio[5]=new Audio();
// lifeAudio[5].src='/sounds/g-major-e4.wav';

lifeAudio[0]=new Audio();
lifeAudio[0].src='/sounds/key1.wav';

lifeAudio[1]=new Audio();
lifeAudio[1].src='/sounds/key2.wav';

lifeAudio[2]=new Audio();
lifeAudio[2].src='/sounds/key3.wav';

lifeAudio[3]=new Audio();
lifeAudio[3].src='/sounds/key4.wav';

lifeAudio[4]=new Audio();
lifeAudio[4].src='/sounds/key5.wav';

lifeAudio[5]=new Audio();
lifeAudio[5].src='/sounds/key6.wav';

var deathAudio = [];
deathAudio[0]=new Audio();
deathAudio[0].src='/sounds/string1.wav';

deathAudio[1]=new Audio();
deathAudio[1].src='/sounds/string2.wav';

deathAudio[2]=new Audio();
deathAudio[2].src='/sounds/string3.wav';

deathAudio[3]=new Audio();
deathAudio[3].src='/sounds/string4.wav';

deathAudio[4]=new Audio();
deathAudio[4].src='/sounds/string5.wav';

deathAudio[5]=new Audio();
deathAudio[5].src='/sounds/string6.wav';

deathAudio[6]=new Audio();
deathAudio[6].src='/sounds/string7.wav';

deathAudio[7]=new Audio();
deathAudio[7].src='/sounds/string8.wav';



console.log(audio);

function playBirthSound() {
var random = Math.floor(Math.random() * 6);
lifeAudio[random].play();
}

function playDeathSound() {
var random = Math.floor(Math.random() * 8);
deathAudio[random].play();
}
